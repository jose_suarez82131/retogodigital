package com.google.translate.commons;


import com.thoughtworks.gauge.AfterSuite;
import com.thoughtworks.gauge.BeforeSuite;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

// CLASE
public class Driver {

    // ATRIBUTOS
    public static WebDriver driver;


    @BeforeSuite
    public void inicializarDriver() {
        WebDriverManager.chromedriver().setup();
        System.setProperty("webdriver.chrome.silentOutput", "true");
        ChromeOptions opciones = new ChromeOptions();
        opciones.addArguments("--start-maximized");
        driver = new ChromeDriver(opciones);
        driver.manage().timeouts().setScriptTimeout(1, TimeUnit.SECONDS);
        imprimirLineaVacia();
    }

    @AfterSuite
    public void cerrarDriver() {
        imprimirLineaVacia();
        driver.quit();
    }

    public void imprimirLineaVacia() { System.out.println("\r\n"); }
}

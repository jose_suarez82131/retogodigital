package com.google.translate.page;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.google.translate.commons.Driver.driver;

public class GoogleTranslatePage {

    private static final By AREA_TO_TRANSLATE = By.xpath("//textarea[@id='source']");
    private static final By AREA_TO_TRANLATED = By.xpath("//div[@class='text-wrap tlid-copy-target']");
    private static final By TO_ENGLISH_BUTTON = By.xpath("//div[@class='tl-sugg']//div[@id='sugg-item-en']");
    private static final By LIST_LANGUAGE = By.xpath("//div[@class='tl-more tlid-open-target-language-list']");
    private static final By SEARCH_LANGUAGE = By.xpath("//input[@id='tl_list-search-box']");

    public static void textToTranslate(String word) {
        WebDriverWait espera = new WebDriverWait(driver, 10);
        espera.until(ExpectedConditions.presenceOfElementLocated(AREA_TO_TRANSLATE));
        driver.findElement(AREA_TO_TRANSLATE).sendKeys(word);
        espera.until(ExpectedConditions.presenceOfElementLocated(TO_ENGLISH_BUTTON));
    }

    public static String textTranslated() {
        WebDriverWait await = new WebDriverWait(driver, 5);
        await.until(ExpectedConditions.presenceOfElementLocated(AREA_TO_TRANLATED));
        System.out.println(driver.findElement(AREA_TO_TRANLATED).getText().trim());
        return driver.findElement(AREA_TO_TRANLATED).getText().trim();
    }

    public static void setLanguage() throws InterruptedException {
        driver.findElement(LIST_LANGUAGE).click();
        driver.findElement(SEARCH_LANGUAGE).sendKeys("Inglés");
        driver.findElement(SEARCH_LANGUAGE).sendKeys(Keys.RETURN);
        TimeUnit.SECONDS.sleep(5);
    }

}

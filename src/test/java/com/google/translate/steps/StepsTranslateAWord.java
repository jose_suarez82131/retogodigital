package com.google.translate.steps;

import com.thoughtworks.gauge.Step;


import static com.google.translate.commons.Driver.driver;
import static com.google.translate.page.GoogleTranslatePage.textToTranslate;
import static com.google.translate.page.GoogleTranslatePage.textTranslated;
import static com.google.translate.page.GoogleTranslatePage.setLanguage;
import static org.assertj.core.api.Assertions.assertThat;


public class StepsTranslateAWord {
    @Step("Go to page <url>")
    public void openPage(String url) {
        driver.navigate().to(url);
    }


    @Step("Input the word <wordToTranslate>")
    public void inputTheWord(String wordToTranslate) {
        textToTranslate(wordToTranslate);
    }

    @Step("Set Up language in english")
    public void setUpLanguage() throws InterruptedException {
        setLanguage();
    }

    @Step("The translate is <wordTranslated>")
    public void theTranslationIs(String wordTranslated) {
        assertThat(wordTranslated).isEqualTo(textTranslated());
    }


}
